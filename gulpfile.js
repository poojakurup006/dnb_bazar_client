var gulp =require("gulp");
var gulphint=require("gulp-jshint");
var changed= require("gulp-changed");
var imagemin =require("gulp-imagemin");
var htmlmin=require("gulp-minify-html");
var concat=require("gulp-concat");
var debug =require("gulp-strip-debug");
var uglify =require("gulp-uglify");
var autoprefix=require("gulp-autoprefixer");
var minifycss =require("gulp-minify-css");
var ngmin = require('gulp-ngmin');
gulp.task("jshint",function(){
	gulp.src('./public/js/*.js').pipe(gulphint()).pipe(gulphint.reporter('default'));
	});
gulp.task("imagemin",function(){
	gulp.src("./public/img/**/*").pipe(changed("./dist/public/img")).pipe(imagemin()).pipe(gulp.dest("./dist/public/img"))
});

gulp.task("htmlmin",function(){
	gulp.src("./public/templates/**/*.html").pipe(changed("./dist/public/templates")).pipe(htmlmin()).pipe(gulp.dest("./dist/public/templates"));
});
gulp.task("scripts",function(){
	gulp.src("./public/js/*.js").pipe(concat('test.js')).pipe(debug()).pipe(ngmin()).pipe(uglify()).pipe(gulp.dest("./dist/public/js"));
	//gulp.src("./public/appjs/*.js").pipe(concat('app.js')).pipe(debug()).pipe(ngmin()).pipe(uglify()).pipe(gulp.dest("./dist/public/js"));
	gulp.src("./public/lib/**/*.js").pipe(concat('lib.js')).pipe(debug()).pipe(gulp.dest("./dist/public/lib"));
});
gulp.task("controller",function(){
	gulp.src("./public/templates/**/*.js").pipe(concat('controller.js')).pipe(debug()).pipe(ngmin()).pipe(uglify()).pipe(gulp.dest("./dist/public/controller"));
});
gulp.task("styles",function(){
gulp.src("./public/css/*.css").pipe(concat("test.css")).pipe(autoprefix('last 2 versions')).pipe(minifycss())
.pipe(gulp.dest('./dist/public/css'));
});
gulp.task("default",["imagemin","htmlmin","styles","scripts","controller"],function(){
	gulp.watch("./public/templates/**/*.html",function(){
		gulp.run('htmlmin');
	});
	gulp.watch(["./public/js/*.js",".public/templates/**/*.js"],function(){
		gulp.run(["scripts","controller"]);
	});
});


