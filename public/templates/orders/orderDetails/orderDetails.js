angular.module('vegetablebazarapp.controllers').controller('orderDetailsCtrl', function($scope,$state,$stateParams,$rootScope,OtherService,$ionicPopup) {

	$scope.cancelButton=false;
    $scope.orderType= $stateParams.orderType;
    var allOrders= $scope.orderType=='current'? $rootScope.currentOrders:$rootScope.orderHistory;

   var order = _.find(allOrders, function(post){ return ((post.id== $stateParams.id) ); });
   $scope.products=JSON.parse(order.order_detail)	;
   $scope.orderid=JSON.parse(order.id);
   
   $scope.orderDate=new Date(order.date);
   $scope.orderDate.setHours(0,0,0,0);
   var today=new Date();
   today.setHours(0,0,0,0);
   var data={'id': $scope.orderid,'date':order.date};

   if( $scope.orderDate!== undefined  && $scope.orderDate!==null && $scope.orderDate!=='' && $scope.orderType=='current')
   {
   	console.log(order.date);
   	console.log(today.getTime());
   		
      if( $scope.orderDate.getTime()===today.getTime() ) 	$scope.cancelButton=true;
   }
   $scope.total=order.total;
   $scope.go=function(){

      OtherService.cancelOrder(angular.toJson(data))
         .then(function(d){
           $scope.vegetables=d.data;$rootScope.currentOrders='';
            $ionicPopup.alert({
              title: 'Success',
              template:"Order is successfully cancelled" });
               
            $state.go('orders');

         }, function(err) { 
            
            $ionicPopup.alert({
              title: 'Error',
              template:"There is an error. Order is not cancelled" + err});
      });

   };
    
});