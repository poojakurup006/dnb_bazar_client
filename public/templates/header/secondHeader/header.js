angular.module('vegetablebazarapp.controllers').controller('HeaderCtrl', function($scope, $location,$window,$timeout,$state,$rootScope,$ionicPopup,OfferService){
	localStorage.hideCart=localStorage.hideCart||true;
    $scope.isDisabled = false;
    $scope.isMessageDisabled = false;
	$scope.isActive = function(route) {
        return route === $location.path();
    };
    $scope.products = [{
                Id: 1,
                Name: 'Dairy'
            }, {
                Id: 2,
                Name: 'Bakery'
            }];

    $scope.product_value = $scope.products[0];

    $scope.GetValue = function () {
    	$rootScope.search_data=$scope.search_data;


    	//alert($rootScope.search_data);
                var productId = $scope.product_value;
                var productName = $.grep($scope.products, function (p) {
                    return p == productId;
                })[0].Name;
                if (productName=='Dairy') {
                	$state.go('vegetables');
                }
                else
                {
                	$state.go('fruits');
                }

    			$rootScope.$broadcast('search', { message: "test" });
				$rootScope.search_data=$scope.search_data;
            };


    $scope.GetOffer = function () {
        $scope.offerinfo=JSON.parse(localStorage.getItem('offerdetails'));
        $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal'));
        $scope.discont=JSON.parse(localStorage.getItem('discont'));
            
    };
    $scope.GetOffer();
    $scope.$on('OfferChange', function (event, args) {
        $scope.GetOffer();     
    });

    
    $scope.offerCheck = function(offer){
        $scope.isDisabled = true;
        var city=window.localStorage.city;
        var offerdata={'offer':offer,'city':city};
        OfferService.getOfferDetails(angular.toJson(offerdata)).then(function(res){
           var info=res;
            $scope.offerdetails=info[0];
            var total=$rootScope.sumofAll;
            var discont;
            if ($scope.offerdetails && $scope.offerdetails.offer_criteria<total) {
            localStorage.setItem('offerdetails', JSON.stringify($scope.offerdetails));
                if($scope.offerdetails.offer_unit === 'rs'){
                    discont=$scope.offerdetails.offer_amt;
                    total=total-($scope.offerdetails.offer_amt);
                    localStorage.setItem('discont', JSON.stringify(discont));
                    $rootScope.sumofAll=total;
                }
                else
                {
                    discont=(total*(($scope.offerdetails.offer_amt)/100));
                    total=total-(total*(($scope.offerdetails.offer_amt)/100)).toFixed(2);
                    localStorage.setItem('discont', JSON.stringify(discont));
                    $rootScope.sumofAll=total;
                }
            
            $rootScope.$broadcast('OfferChange', { message: "test" });
        }
        else{
                $scope.isMessageDisabled = true;
                $scope.errorMessage="This Offer is not applicable. Please try again later.";
                $scope.isDisabled = false;
                $timeout(function () { $scope.isMessageDisabled = false;$scope.errorMessage=''; }, 3000);
        
        }

        },function(err){console.log("There is an error ");}) ;

    };
});