angular.module('vegetablebazarapp.controllers').controller('ChangeHeaderCtrl', function($scope, $state, ngDialog, OtherService,ContactInfoService,$rootScope,$ionicPopup){
	var data=window.localStorage.city;
	$rootScope.city=data;
	$scope.name=localStorage.displayName;
	$scope.currentuser=angular.toJson($rootScope.globals);
	//alert($scope.currentuser);

	if ($rootScope.globals.currentUser!=='' )
	{
		var self=this;
		self.getBalance=function(){

			OtherService.getBalance(angular.toJson($rootScope.globals.currentUser.username))
			.then(function(res){
				$scope.balance=res[0].balance;
			}, function(err) { 
				console.log("error"+data);
			});
		};
		self.getBalance();
		$scope.$on('priceChange', function (event, args) {
			self.getBalance();	   
		});
	}

	if (!window.localStorage.city) {
	
		ngDialog.open({
	        template: 'templates/selectCity/selectCity.html',
	        className: 'ngdialog-theme-default',
	        appendClassName: 'ngdialog-selectCity',
	        controller: 'SelectCityCtrl',
	        showClose: false,
	        closeByDocument: false,
	        closeByEscape: false
	    });
    }

    $scope.showDialogBox=function(disableCity)
    {
    	
    	if (!disableCity) {

    	ngDialog.open({
	        template: 'templates/selectCity/selectCity.html',
	        className: 'ngdialog-theme-default',
	        appendClassName: 'ngdialog-selectCity',
	        controller: 'SelectCityCtrl',
	        showClose: false,
	        closeByDocument: false,
	        closeByEscape: false
	    });

    	}
    }
    $scope.ContactInfo=[];
    GetAllContactInfo=function()
    {
      ContactInfoService.getContactInfo('/getContactInfo').then(function(res){
    		var info=res;
			$scope.ContactInfo=info[0];
			if (!$scope.ContactInfo) {
				$scope.ContactInfo={"email":"support@dairynbakerybazar.com","mobile":"7060339060"};
			}

		},function(err){console.log("There is an error ");$scope.ContactInfo={"email":"support@dairynbakerybazar.com","mobile":"7060339060"};}) ;
    }
    GetAllContactInfo();

    $scope.$on('cityChange', function (event, args) {

    	GetAllContactInfo();
    });

});