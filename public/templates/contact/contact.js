angular.module('vegetablebazarapp.controllers').controller('ContactCtrl', function($scope,$rootScope, $ionicPopup, AuthService) {
$rootScope.vegfruitname='Contact Us';

$scope.sendMessage = function(){
  

		AuthService.sendMessage(angular.toJson($scope.user))
			.then(function(data){
				
				$ionicPopup.alert({
			     title: 'Successfull',
			     template:"You Message has send successfully "});

				$scope.user.name='';
				$scope.user.email='';
				$scope.user.mobile='';
				$scope.user.message='';

				
				//$scope.$broadcast('scroll.refreshComplete');
			}, function(err) { 
				
				$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});
		
		
	};

});