angular.module('vegetablebazarapp.controllers').controller('SelectCityCtrl', function(_,$scope, $state, ngDialog, $http, $rootScope,SERVER_URL,CartService){
	GetAllMasterCityName=function()
    {
      $http.get(SERVER_URL+'/getMasterCities')
                            .then(
                                    function(response){
                                        $scope.mastercityName= response.data;
                                      $scope.selectedmastercity = $scope.mastercityName[0];
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    }
    GetAllMasterCityName();
    GetAllCityName=function()
    {
      $http.get(SERVER_URL+'/GetCityName')
                            .then(
                                    function(response){
                                        $scope.allCity= response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    }
    GetAllCityName();
    $scope.mastercitychange=function(data)
    {        
        $scope.cityName =  _.where( $scope.allCity, { master_city_id:parseInt(data)} );
        $scope.selectedcity = $scope.cityName[0];

    }

    $scope.change=function(data)
    {
    	//alert(angular.toJson(data));
    	window.localStorage.city=data;
    	$rootScope.city=data;
    	ngDialog.close();
    	$rootScope.$broadcast('cityChange', { message: "test" });
        CartService.CartChange();
        $rootScope.$broadcast('priceChange', { message: "test" });

    }
});