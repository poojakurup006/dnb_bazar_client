angular.module('vegetablebazarapp.controllers').controller('profileCtrl', function($scope, $state,CartService,address_data, States_data, Master_City_data, Cities_data,Areas_data,Status_data, $timeout,$window, $ionicLoading, $ionicPopup, AuthService) {
$scope.isPassDisabled = false;
$scope.isAddressDisabled = false;
	
$scope.user=$scope.newaddress? {}:address_data;
	  
	      $scope.states =  States_data  || [
	        { id: 1, name: 'Maharashtra' },
	        { id: 2, name: 'MP' },
	        { id: 3, name: 'Gujarat' },
	        { id: 4, name: 'UP' }
	      ];
	   	  $scope.masterCity =  Master_City_data  || [
	        { id: 1, name: 'Uttrakhand' },
	        { id: 2, name: 'Uttrakhand1' },
	        { id: 3, name: 'Uttrakhand2' },
	        { id: 4, name: 'Uttrakhand3' },
	      ];
	      $scope.citys = Cities_data  || [
	        { id: 1, name: 'Pune' },
	        { id: 2, name: 'Mumbai' },
	        { id: 3, name: 'Nagpur' },
	        { id: 4, name: 'Nasik' }
	      ];
	      $scope.Areas = Areas_data  || [
	        { id: 1, name: 'Area 1' },
	        { id: 2, name: 'Area 2' },
	        { id: 3, name: 'Area 3' },
	        { id: 4, name: 'Area 4' }
	      ];
	      $scope.Status = Status_data  || [
	        { id: 1, name: 'test1' },
	        { id: 2, name: 'Housewife' },
	        { id: 3, name: 'Working' },
	        { id: 4, name: 'Commercial' }
	      ];
	     $scope.masterCity =  _.where( Master_City_data, { state_id:parseInt($scope.user.state)} );
	     $scope.citys =  _.where( Cities_data, { master_city_id:parseInt($scope.user.master_city)} );
	     $scope.Areas =  _.where( Areas_data, { city_id:parseInt($scope.user.city)} );
	 $scope.GetSelectedMasterCity=function(state){$scope.masterCity =  _.where( Master_City_data, { state_id:parseInt(state)} );};
 $scope.GetSelectedCities=function(mastercity){$scope.citys =  _.where( Cities_data, { master_city_id:parseInt(mastercity)} );};
 $scope.GetSelectedAreas=function(city){  $scope.Areas =  _.where( Areas_data, { city_id:parseInt(city)} );};
	        

$scope.changePassword = function(){
$scope.isPassDisabled = true;

	
	if($scope.user.password===$scope.user.originalPassword){
		delete $scope.user.confirmpassword;
		

			AuthService.changePassword(angular.toJson($scope.user))
				.then(function(data){
					 if(data.affectedRows && data.affectedRows!==0){
						$scope.editAddress=false;
			           
						$ionicPopup.alert({
				     title: 'Success',
				     template:"Password Changed Successfully" });
					$scope.isPassDisabled = false;						
						$state.go('profile', {}, { reload: true });
					}
				}, function(err) { 
					
					$ionicPopup.alert({
				     title: 'Error',
				     template:"There is an error.. Please try again " + err});
					$scope.isPassDisabled = false;
			});
		}
				else{
					$ionicPopup.alert({
				     title: 'Error',
				     template:"There is an error.. Please try again " });
					$scope.isPassDisabled = false;
				}
};
	$scope.saveAddress = function(){
		$scope.isAddressDisabled = true;

		AuthService.saveAddress(angular.toJson($scope.user))
			.then(function(data){              
					
					 if(data.affectedRows && data.affectedRows!==0){
						$scope.editAddress=false;
						$ionicPopup.alert({
				     title: 'Success',
				     template:"Address information Changed Successfully" });
						$scope.isAddressDisabled = false;
						$state.go('profile', {}, { reload: true });
					}
					
			}, function(err) { 
				
				$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
				$scope.isAddressDisabled = false;
		});
		
		
	};
})