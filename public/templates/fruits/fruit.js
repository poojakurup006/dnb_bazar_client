angular.module('vegetablebazarapp.controllers').controller('fruitsListCtrl', function($rootScope,$scope,$timeout,$ionicPopup,CartService,_) {

$scope.search_data=$rootScope.search_data;


 	$scope.$on('search', function (event, args) {		
		$scope.search_data=$rootScope.search_data;
		$rootScope.search_data='';	   
	});

$rootScope.search_data='';

$scope.fruits=!_.isUndefined(window.localStorage.lsallFruits) ? JSON.parse(window.localStorage.lsallFruits) : [];

	$rootScope.vegfruitname='Bakery';
	$scope.fru=[];	

	CartService.getFruits()
			.then(function(d){
				//console.log(JSON.stringify(d));
				window.localStorage.lsallFruits=JSON.stringify(d);
                $scope.fruits=d;

                CartService.getListPrices('/getFruitCityPrices',$scope.fruits).then(function(res){  
                $scope.fru=[];
                $scope.fruits=res;
                angular.forEach($scope.fruits, function(fru){
                    if(fru.price>0){
                        $scope.fru.push(fru);
                    }
                });
                },function(err){
					console.log("error occured"+err);
				});

			}, function(err) { 
				console.error('Error while fetching fruitlist');
		});

 	$scope.$on('priceChange', function (event, args) {
		 $scope.message = args.message;
		  CartService.getListPrices('/getFruitCityPrices',$scope.fruits).then(function(res){ 
		  	$scope.fruits=res;
		  	$scope.fru=[];
		  	angular.forEach($scope.fruits, function(fru){
               if(fru.price>0){
                 $scope.fru.push(fru);
               }
             });
		  },function(err){
 	console.log("error occured"+err);});
		   //$scope.$emit('CartChange', { priceList: $scope.fruits });
 });	

 	$scope.$on('cityChange', function (event, args) {
		 $scope.message = args.message;
		  CartService.getListPrices('/getFruitCityPrices',$scope.fruits).then(function(res){ 
		  	$scope.fruits=res;
		  	$scope.fru=[];
		  	angular.forEach($scope.fruits, function(fru){
               if(fru.price>0){
                 $scope.fru.push(fru);
               }
             });
		  },function(err){
 	console.log("error occured"+err);});
		   //$scope.$emit('CartChange', { priceList: $scope.fruits });
 });

	$scope.cart=[];
		$scope.showPopup = function(id,name,hindi_name,img,price,quan,unit) {
		 $scope.data = {};
		 $scope.custom=false;
		 $scope.data.id=id;
		 $scope.data.name=name;
		 $scope.data.hindi_name=hindi_name;
		 $scope.data.image=img;
		 $scope.data.price=price;
		 $scope.data.quan=quan;
		 $scope.data.unit=unit;

		 if (($scope.data.quan === 'kg') || ($scope.data.quan === 'gram')) {
		 	$scope.quantityList=[{'name':'0.25','value':0.25},{'name':'0.5','value':0.5},{'name':'0.75','value':0.75},{'name':'1','value':1},
						{'name':'1.5','value':1.5},{'name':'2','value':2},{'name':'2.5','value':2.5},{'name':'3','value':3},{'name':'3.5','value':3.5},{'name':'4','value':4},{'name':'4.5','value':4.5},{'name':'5','value':5}];

		 }
		 else{
		 	$scope.quantityList=[{'name':'1','value':1},{'name':'2','value':2},{'name':'3','value':3},{'name':'4','value':4},
					   {'name':'5','value':5},{'name':'6','value':6},{'name':'7','value':7},{'name':'8','value':8},{'name':'9','value':9},{'name':'10','value':10}];
		 
		 }

		
							   
		 var eitem=CartService.checkExistingItem($scope.data);
		 if(eitem){$scope.data=eitem; }
		 
		  // An elaborate, custom popup
		  if (($scope.data.quan === 'kg') || ($scope.data.quan === 'gram')) {
		  	var myPopup = $ionicPopup.show({
		    template: '<style type="text/css">.item-select {width: 100%;}.item-select select {left: 0;}'+
		           '</style><div ><ion-toggle toggle-class="toggle-energized" ng-model="custom" ng-click="toggleClick($event)">Custom Quantity</ion-toggle>'+
		               ' <select ng-if="!custom" ng-model="data.quantity" ng-selected="{{quantityList.value==data.quantity}}" ng-options="i.value as i.value for i in quantityList" class="myselectstyle"><option value="">Select a quantity</option>'+
		                '</select> <input  type=number name="qty" min=".25"  class="myselectstyle" placeholder="250gm & above" ng-if="custom" ng-pattern="/\d*\.?\d*/" ng-model="data.quantity"></input >'+
		                '<span> {{data.quan}} </span>'+
		                 ' </div> '+
		                 '<script>var myfunction = function(e) {if(!((e.keyCode > 99 && e.keyCode < 106)|| (e.keyCode > 51 && e.keyCode < 58) || e.keyCode == 8 )) {console.log(e.keyCode);return false;}}</script>',
		    cssClass: 'my-custom-popup',
		    title: 'Enter Quantity of ' + name,
		    scope: $scope,
		    buttons: [
				      { text: '<b>Cancel</b>',
				      	type: 'btn-danger', },
				      {
				        text: '<b>Save</b>',
				        type: 'btn-success',
				        onTap: function(e) {
				        	
				        	
				          if (!$scope.data.quantity ) {

				            e.preventDefault();

				          }
				           else {
				          	
				          
				          	CartService.addCartItem($scope.data);
				          	console.log(angular.toJson("fgfhgjgukuhjh"+$scope.data));
				          	$rootScope.$broadcast('CartChange', { message: "test" });

				          }
		        }
		      }
		    ]
		  });
		  }
		  else{
		  	var myPopup = $ionicPopup.show({
		    template: '<style type="text/css">.item-select {width: 100%;}.item-select select {left: 0;}'+
           		'</style><form name="cartvalue"><div ><ion-toggle toggle-class="toggle-energized" ng-model="custom" ng-click="toggleClick($event)" class="borderdesign">Custom Quantity</ion-toggle>'+
             	' <select ng-if="!custom" ng-model="data.quantity" ng-selected="{{quantityList.value==data.quantity}}" ng-options="i.value as i.value for i in quantityList" class="myselectstyle"><option value="">Select a quantity</option>'+
                '</select>  <input  type="number" name="quantity" min="1" step="1"  ng-pattern="/^[0-9]{1,4}$/" class="myselectstyle" placeholder="1 Item & above" ng-if="custom" ng-model="data.quantity" style="padding-left:10px;"></input >'+
		         '<span> {{data.quan}} </span>'+
		         '<span class="invalid" ng-show="cartvalue.quantity.$error.pattern">Please enter valid number</span>'+
		         ' </div> </form>'+
                 '<script>var myfunction = function(e) {if(!((e.keyCode > 99 && e.keyCode < 106)|| (e.keyCode > 51 && e.keyCode < 58) || e.keyCode == 8 )) {console.log(e.keyCode);return false;}}</script>',
    		cssClass: 'my-custom-popup',
		    title: 'Enter Quantity of ' + name,
		    scope: $scope,
		    buttons: [
		      { text: '<b>Cancel</b>',
		      	type: 'btn-danger', },
		      {
		        text: '<b>Save</b>',
		        type: 'btn-success',
		        onTap: function(e) {
		        	
		        	
		          if (!$scope.data.quantity ) {

		            e.preventDefault();

		          }
		           else {
		          	
		          
		          	CartService.addCartItem($scope.data);
		          	console.log(angular.toJson("fgfhgjgukuhjh"+$scope.data));
		          	$rootScope.$broadcast('CartChange', { message: "test" });

		          }
		        }
		      }
		    ]
		  });


		  }

		  
		};	
});