angular.module('vegetablebazarapp.controllers').controller('subscriptionDetailCtrl', function($scope, $http,$window,$stateParams,$timeout, $filter,$ionicLoading, $rootScope,$ionicPopup, AuthService, SubscriptionService,SERVER_URL) {
 
$scope.milk={};

$scope.subid=$stateParams.id;

$scope.lastDay='';
var lastdate;
var firstdate;


DisplaySubDetails = function(){
$scope.allsubdata=(angular.fromJson($window.localStorage.subscriptiondata));
$scope.allsubdata.forEach(function(entry)
{
	if(entry.id==$scope.subid){
		$scope.milk=entry;

		var between=[];
		var currentDate = new Date($scope.milk.start_date),end = new Date($scope.milk.end_date);
        while (currentDate <= end) {
            between.push(new Date(moment(currentDate).startOf('day')));
            currentDate.setDate(currentDate.getDate() + 1);
        }
        //alert(angular.toJson(currentDate));
        firstdate=new Date($scope.milk.start_date);
        lastdate=new Date($scope.milk.end_date);

		//$scope.day_count=between.length;
		//$scope.total_qty =  $scope.milk.quantity * $scope.milk.frequency * $scope.day_count;
        ExceptionDetails(between);
	}
});
};

ExceptionDetails = function(alldates){
	var edata=[];

	var data= angular.toJson($scope.subid);
	var req={method: 'GET',url: SERVER_URL+'/getException',headers: {'Content-Type': 'application/json',data:data}};
	$http(req).success(function (d) {	

                    window.localStorage.exceptiondata=JSON.stringify(d);
                    console.log("all exception date"+ angular.toJson(d));
                    d.forEach(function(entry){

                    	 var t= new Date(entry.exception_date).setHours(0,0,0,0);                    

                    	 alldates.forEach( function(p){ 
                    	 	var test=new Date(entry.exception_date).setHours(0,0,0,0);
                    	 	console.log(test);
                    	 	if(p.getTime() == (test)) {

                    			alldates.splice(alldates.indexOf(p),1);
                    		}
                    	 });
                    	
                    });
                   calanderDates(alldates);
                    
		}).error( function(data){
			$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
		});
	
};
$scope.$on('updateSubData', function(){
	SubscriptionService.getSubcriptionDetails().then(function(data){
				
	DisplaySubDetails();});
	//console.log("exceptiondata");
});

 var todaysdate = $filter('date')(new Date(), 'yyyy-MM-d');
 var td=new Date(todaysdate);
 var ttd = td.setDate(td.getDate() + 1);
 var tomorwdate = $filter('date')(ttd, 'yyyy-MM-d');

function calanderDates (daterange){
	$scope.myArrayOfDates=[];
	$scope.day_count=daterange.length;
	$scope.total_qty =  $scope.milk.quantity * $scope.milk.frequency * $scope.day_count;
	//alert(angular.toJson(daterange));

	console.log("exception"+angular.toJson(daterange));
	daterange.forEach(function(d){
		
		$scope.myArrayOfDates.push(moment(d).date(d.getDate()).month(d.getMonth()));

	});
	 if( firstdate >  new Date(todaysdate))
	 {
	 	$scope.today = (moment(firstdate).date(firstdate.getDate()).month(firstdate.getMonth()));
	 }
	 else{
	  	$scope.today = moment();	
	 }
	$scope.lastDay = (moment(lastdate).date(lastdate.getDate()).month(lastdate.getMonth()));

}

$scope.myArrayOfDates = [moment().date(4)];
$scope.$watch('myArrayOfDates', function(newValue, oldValue){
    if(newValue){
        console.log('my array changed, new size : ' + newValue.length);
    }
}, true);


 $scope.selected_dates=[];
 $scope.excpdeldate=[];
 $scope.checkSelection = function(event, date) {
 	
    
    var testDate=date.date;
	var test = new Date(testDate);
	var day=test.getDate();
	var month=test.getMonth()+1;
	var year=test.getFullYear();
	var cale_date= year + "-" + month + "-" + day;
	var dateclicked=cale_date;

	if( new Date(dateclicked) >  new Date($scope.milk.end_date) || new Date(dateclicked) < new Date($scope.today)) return;
	if((dateclicked === todaysdate) || (dateclicked === tomorwdate))  return $ionicPopup.alert({title: 'Sorry !!!', template:"You cannot remove Today and Tomorrows subscription ..."});

	if(date.mdp.selected){
	$scope.selected_dates.push(dateclicked);}
	else{
		$scope.selected_dates.forEach(function(d){

			if (d==dateclicked){ 

			$scope.selected_dates.splice($scope.selected_dates.indexOf(d),1);
		}});
	}
	if (!date.mdp.selected) {
		$scope.excpdeldate.push(dateclicked);
	}
	else{
		$scope.excpdeldate.forEach(function(d){

			if (d==dateclicked){ 

			$scope.excpdeldate.splice($scope.excpdeldate.indexOf(d),1);
		}});
	}
	
};

$scope.saveexceptiondate = function(){
		$ionicLoading.show({
			template: 'Adding exception date...'
		});
		$scope.arraydate=[];
		$scope.selected_dates.forEach(function(d)
		{
			$scope.arraydate.push(d);
		});
		$scope.excpdeldate.forEach(function(d)
		{
			$scope.arraydate.push(d);
		});
		//$scope.arraydate={activeDate: $scope.selected_dates, deletedDate: $scope.excpdeldate};

		AuthService.saveExceptionDate($scope.arraydate,$scope.milk.id)
			.then(function(data){
				
				//$rootScope.$broadcast('updateSubDateData');
				$rootScope.$broadcast('updateSubData');
				setTimeout(function() {
					DisplaySubDetails();
				}, 1000);
				$scope.selected_dates=[];
 				$scope.excpdeldate=[];
				
               $ionicLoading.hide();
				$ionicPopup.alert({
			     title: 'Success',
			     template:"Exception date added successfully"});
				
				

					 
			}, function(err) { 
				
				$ionicLoading.hide();$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});


};


DisplaySubDetails();

	
});