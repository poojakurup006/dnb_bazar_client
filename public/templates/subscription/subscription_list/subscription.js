angular.module('vegetablebazarapp.controllers').controller('subscriptionListCtrl', function($scope, $window,$filter,$q,$http,$rootScope, $timeout, $ionicLoading, $ionicPopup,SERVER_URL,SubscriptionService,ContactInfoService) {
		$scope.subdata=[];
		var data= angular.toJson($rootScope.globals.currentUser.username);

		//alert(angular.toJson(data));
		getSubcriptionDetails=function(){
			SubscriptionService.getSubcriptionDetails().then(function(data){
				var suballdata=data[0];
				var todaysdate = $filter('date')(new Date(), 'yyyy-MM-d');
				$scope.subdata=[];
				suballdata.forEach(function(entry){
				var LastAvailableDate = entry.end_date;
				if( new Date(todaysdate) <= new Date(LastAvailableDate))
				{
					$scope.subdata.push(entry);
				}


				});

			});
			
		};
getSubcriptionDetails();
var self=this;
$scope.ContactInfo=[];
self.getContactCityDetails=function(){
	var deferred = $q.defer();
		var data= angular.toJson($rootScope.globals.currentUser.username);
		var req={method: 'GET',url: SERVER_URL+'/getContactCityDetails',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			 $scope.city=res[0].cityname;
			
			ContactInfoService.getContactInfo($scope.city).then(function(res){
    		var info=res;
			$scope.ContactInfo=info[0];
			if (!$scope.ContactInfo) {
				$scope.ContactInfo={"email":"support@dairynbakerybazar.com","mobile":"7060339060"};
			}

		},function(err){console.log("There is an error ");$scope.ContactInfo={"email":"support@dairynbakerybazar.com","mobile":"7060339060"};}) ;
		}).error( function(data){
			deferred.reject(data); 
			console.log("There is an error ");$scope.ContactInfo={"email":"support@dairynbakerybazar.com","mobile":"7060339060"};
		});
            //}, 1000);
          return deferred.promise;
};
self.getContactCityDetails();
$scope.$on('updateSubData', function(){
	console.log("subdetail"+ new Date());
	getSubcriptionDetails();
	
});



});