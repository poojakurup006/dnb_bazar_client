angular.module('vegetablebazarapp.controllers').controller('RechrgeCtrl', function($scope,$rootScope, $q,SERVER_URL,$ionicPopup,$http,AuthService) {
$rootScope.vegfruitname='Recharge Wallet';
$scope.isDisabled = false;
$scope.paymentMethodList = [
    { text: "Cash", value: "CA" },
    { text: "Cheque(Min 500)", value: "CQ" },
    { text: "Add Bank Details to Transfer Online", value: "BA" }
  ];

 $scope.paymentmethod=false;
  $scope.GetSelectedPaymentMethod=function(value){
	$scope.paymentmethod=false;
  	if (value == 'BA') {
  		$scope.paymentmethod=true;
  	}
  	else
  	{
  		$scope.paymentmethod=false;
  	}
  };
  $scope.bank={};
  $scope.getPaymentMethod = function(){
		 var deferred = $q.defer();
		var data= angular.toJson($rootScope.globals.currentUser.username);
		var req={method: 'GET',url: SERVER_URL+'/getPaymentMethod',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			var data=res[0];
			$scope.paymethod=data.payment_method;
			if ($scope.paymethod == 'BA') {
		  		$scope.paymentmethod=true;
		  	}

		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
		});
            //}, 1000);
          return deferred.promise;
		
	};
	$scope.getPaymentMethod();

  $scope.getBankDetails = function(){
		 var deferred = $q.defer();
		var data= angular.toJson($rootScope.globals.currentUser.username);
		var req={method: 'GET',url: SERVER_URL+'/getBankDetails',headers: {'Content-Type': 'application/json',data:data}};
		$http(req).success(function (res) {
			$scope.bank=res[0];

		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
		});
            //}, 1000);
          return deferred.promise;
		
	};
	$scope.getBankDetails();
	$scope.UpdatePaymentType=function(value){
		$scope.isDisabled = true;
		var deferred = $q.defer();
		var data= {'mobile':$rootScope.globals.currentUser.username,'payment_method':value};
		var req={method: 'GET',url: SERVER_URL+'/updatePaymentMethod',headers: {'Content-Type': 'application/json',data:angular.toJson(data)}};
		$http(req).success(function (res) {
			$ionicPopup.alert({
			     title: 'Success',
			     template:"Successfully saved payment method "});
			$scope.isDisabled = false;

		}).error( function(data){
			deferred.reject(data); $ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + data});
			$scope.isDisabled = false;
		});
            //}, 1000);
          return deferred.promise;
  };

});