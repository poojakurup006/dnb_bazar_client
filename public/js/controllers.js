angular.module('vegetablebazarapp.controllers', [])


.controller('BalanceCtrl', function($scope,$rootScope,OtherService,$ionicPopup) {
	var self=this;
	self.getBalance=function(){
		OtherService.getBalance(angular.toJson($rootScope.globals.currentUser.username))
			.then(function(res){
				$scope.balance=res[0].balance;
			}, function(err) { 				
				$ionicPopup.alert({
			     title: 'Error',
			     template:"There is an error.. Please try again " + err});
		});
	};
	self.getBalance();
	$scope.$on('priceChange', function (event, args) {
		self.getBalance();	   
	});
})


.controller("ShoppingCartCtrl", ["$scope","$rootScope","$state", "CartService", "$ionicActionSheet", "$timeout","_", function($scope,$rootScope,$state, CartService, $ionicActionSheet,$timeout, _) {
    $scope.products = CartService.getCartItems();
    $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal'));
    $scope.$on('CartChange',function(event, args)
    {
    	$scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal'));
        $scope.products = CartService.getCartItems();

    });
    $scope.sweet = {};
        $scope.sweet.option = {
            title: "Are you sure?",
            text: "You want to remove this product!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, keep it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }
        $scope.sweet.confirm = {
            title: 'Deleted!',
            text: 'Successfully deleted.',
            type: 'success'
        };

        $scope.sweet.cancel = {
            title: 'Cancelled!',
            text: 'Product not deleted.',
            type: 'error'
        }
        
        $scope.checkCancel=function(){
        return !0;
        }
        
         $scope.checkConfirm=function(test){
          return CartService.removeCartItem(test), $scope.products = CartService.getCartItems(), $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal')), !0;
        }

       $scope.roundToTwo= function (num) {    
    			return +(Math.round(num + "e+2")  + "e-2");
		};
    $scope.$on('priceChange', function (event, args) {

		CartService.CartChange().then(function(res){console.log(res);
            setTimeout(function() {$scope.$apply(function(){ 
                    $scope.products = CartService.getCartItems(); 
                    $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal')); 
                });}, 100);
        },function(err){console.log("there is an error"+err);});

	});
    $scope.$on('loginChange', function (event, args) {

            setTimeout(function() {$scope.$apply(function(){ 
                    $scope.products = CartService.getCartItems(); 
                    $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal')); 
                });}, 100);

    });

    $scope.go = function() {
    	if(!$rootScope.globals.currentUser.username)
    		{
    			$state.go('signin');
    		}
    		else
    		{
    			$state.go('shipping');

    		}
    	};

}])


.controller("LogoutCtrl", ["$scope","$rootScope","AuthService","$state","$timeout", "CartService", "_", function($scope,$rootScope,AuthService,$state,$timeout, CartService, _) {
    
  var logout=function() {AuthService.ClearCredentials();
  	
  	localStorage.removeItem('lsallCartItems');
    localStorage.removeItem('offerdetails');
    localStorage.removeItem('discont');
    $rootScope.currentOrders='';
    localStorage.removeItem('lsallFruits');
    localStorage.removeItem('lsallVegetables');
    localStorage.removeItem('globals');
    $rootScope.sumofAll=0;
    CartService.CartChange();
    $rootScope.disableCity=false;
    $state.go('auth');
};

$timeout(function () {logout();},100);

}])
   
;
